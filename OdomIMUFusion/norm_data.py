import os.path
import sys
import pandas as pd
import numpy as np

def norm_data():
	odom_pos_x_mean = 0
	odom_pos_x_std = 0
	odom_pos_y_mean =0
	odom_pos_y_std = 0
	odom_vel_x_mean = 0
	odom_vel_x_std = 0
	odom_vel_y_mean = 0
	odom_vel_y_std = 0
	odom_ang_z_mean = 0
	odom_ang_z_std = 0
	imu_ang_x_mean = 0
	imu_ang_x_std = 0
	imu_ang_y_mean = 0
	imu_ang_y_std = 0
	imu_ang_z_mean = 0
	imu_ang_z_std = 0
	imu_accel_x_mean = 0
	imu_accel_x_std = 0
	imu_accel_y_mean = 0
	imu_accel_y_std = 0
	myFields = ['odom_pos_x',
		'odom_pos_y',
		'odom_quat_x',
		'odom_quat_y',
		'odom_quat_z',
		'odom_quat_w',
		'odom_vel_x',
		'odom_vel_y',
		'odom_ang_z',
		'imu_quat_x',
		'imu_quat_y',
		'imu_quat_z',
		'imu_quat_w',
		'imu_ang_x' ,
		'imu_ang_y' ,
		'imu_ang_z' ,
		'imu_accel_x',
		'imu_accel_y' ]
	i = 0
	inpath = sys.argv[1]+str(i)+".csv"
	outpath = sys.argv[2]+str(i)+".csv"
	while os.path.exists(inpath):
		
		data = pd.read_csv(inpath, header=0)

		odom_pos_x = data['odom_pos_x']
		odom_pos_y = data['odom_pos_y']

		odom_quat_x = data['odom_quat_x']
		odom_quat_y = data['odom_quat_y']
		odom_quat_z = data['odom_quat_z']
		odom_quat_w = data['odom_quat_w']

		odom_vel_x = data['odom_vel_x']
		odom_vel_y = data['odom_vel_y']
		odom_ang_z = data['odom_ang_z']

		imu_quat_x = data['imu_quat_x']
		imu_quat_y = data['imu_quat_y']
		imu_quat_z = data['imu_quat_z']
		imu_quat_w = data['imu_quat_w']

		imu_ang_x = data['imu_ang_x']
		imu_ang_y = data['imu_ang_y']
		imu_ang_z = data['imu_ang_z']
		imu_accel_x = data['imu_accel_x']
		imu_accel_y = data['imu_accel_y']


		if i==0:
			odom_pos_x_mean = np.mean(odom_pos_x)
			odom_pos_x_std = np.std(odom_pos_x, ddof=1)
			odom_pos_y_mean = np.mean(odom_pos_y)
			odom_pos_y_std = np.std(odom_pos_y, ddof=1)
			odom_vel_x_mean = np.mean(odom_vel_x)
			odom_vel_x_std = np.std(odom_vel_x, ddof=1)
			odom_vel_y_mean = np.mean(odom_vel_y)
			odom_vel_y_std = np.std(odom_vel_y, ddof=1)
			odom_ang_z_mean = np.mean(odom_ang_z)
			odom_ang_z_std = np.std(odom_ang_z, ddof=1)
			imu_ang_x_mean = np.mean(imu_ang_x)
			imu_ang_x_std = np.std(imu_ang_x, ddof=1)
			imu_ang_y_mean = np.mean(imu_ang_y)
			imu_ang_y_std = np.std(imu_ang_y, ddof=1)
			imu_ang_z_mean = np.mean(imu_ang_z)
			imu_ang_z_std = np.std(imu_ang_z, ddof=1)
			imu_accel_x_mean = np.mean(imu_accel_x)
			imu_accel_x_std = np.std(imu_accel_x, ddof=1)
			imu_accel_y_mean = np.mean(imu_accel_y)
			imu_accel_y_std = np.std(imu_accel_y, ddof=1)
			stats = pd.DataFrame({'odom_pos_x_mean' : odom_pos_x_mean,
						'odom_pos_x_std' :  odom_pos_x_std,
						'odom_pos_y_mean' :  odom_pos_y_mean,
						'odom_pos_y_std' : odom_pos_y_std,
						'odom_vel_x_mean' :  odom_vel_x_mean,
						'odom_vel_x_std' : odom_vel_x_std,
						'odom_vel_y_mean' : odom_vel_y_mean,
						'odom_vel_y_std' : odom_vel_y_std,
						'odom_ang_z_mean' : odom_ang_z_mean,
						'odom_ang_z_std' : odom_ang_z_std,
						'imu_ang_x_mean' : imu_ang_x_mean,
						'imu_ang_x_std' : imu_ang_x_std,
						'imu_ang_y_mean' : imu_ang_y_mean,
						'imu_ang_y_std' :  imu_ang_y_std,
						'imu_ang_z_mean' : imu_ang_z_mean,
						'imu_ang_z_std' : imu_ang_z_std,
						'imu_accel_x_mean' : imu_accel_x_mean,
						'imu_accel_x_std' :  imu_accel_x_std,
						'imu_accel_y_mean' : imu_accel_y_mean,
						'imu_accel_y_std' :  imu_accel_y_std}, index=[0])
			stats.to_csv(sys.argv[2]+"stats.csv" ,index=False)


		
		odom_pos_x = (odom_pos_x - odom_pos_x_mean) / odom_pos_x_std
		odom_pos_y = (odom_pos_y - odom_pos_y_mean) / odom_pos_y_std

		odom_vel_x = (odom_vel_x - odom_vel_x_mean) / odom_vel_x_std
		odom_vel_y = (odom_vel_y - odom_vel_y_mean) / odom_vel_y_std
		odom_ang_z = (odom_ang_z - odom_ang_z_mean) / odom_ang_z_std
		

		imu_ang_x = (imu_ang_x - imu_ang_x_mean) / imu_ang_x_std
		imu_ang_y = (imu_ang_y - imu_ang_y_mean) / imu_ang_y_std
		imu_ang_z = (imu_ang_z - imu_ang_z_mean) / imu_ang_z_std

		imu_accel_x = (imu_accel_x - imu_accel_x_mean) / imu_accel_x_std
		imu_accel_y = (imu_accel_y - imu_accel_y_mean) / imu_accel_y_std


		new_csv = pd.DataFrame({'odom_pos_x' : odom_pos_x,
					'odom_pos_y' :  odom_pos_y,
					'odom_quat_x' :  odom_quat_x,
					'odom_quat_y' : odom_quat_y,
					'odom_quat_z' :  odom_quat_z,
					'odom_quat_w' : odom_quat_w,
					'odom_vel_x' : odom_vel_x,
					'odom_vel_y' : odom_vel_y,
					'odom_ang_z' : odom_ang_z,
					'imu_quat_x' : imu_quat_x,
					'imu_quat_y' : imu_quat_y,
					'imu_quat_z' : imu_quat_z,
					'imu_quat_w' : imu_quat_w,
					'imu_ang_x' :  imu_ang_x,
					'imu_ang_y' : imu_ang_y,
					'imu_ang_z' : imu_ang_z,
					'imu_accel_x' : imu_accel_x,
					'imu_accel_y' :  imu_accel_y })

		new_csv.to_csv(outpath, columns = myFields,index=False)
		i = i + 1
		inpath = sys.argv[1]+str(i)+".csv"
		outpath = sys.argv[2]+str(i)+".csv"


if __name__ == '__main__':
	norm_data()