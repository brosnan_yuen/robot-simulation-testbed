import os.path
import sys
import pandas as pd
import numpy as np

def add_noise():
	myFields = ['odom_pos_x',
		'odom_pos_y',
		'odom_quat_x',
		'odom_quat_y',
		'odom_quat_z',
		'odom_quat_w',
		'odom_vel_x',
		'odom_vel_y',
		'odom_ang_z',
		'imu_quat_x',
		'imu_quat_y',
		'imu_quat_z',
		'imu_quat_w',
		'imu_ang_x' ,
		'imu_ang_y' ,
		'imu_ang_z' ,
		'imu_accel_x',
		'imu_accel_y' ]
	i = 0
	inpath = sys.argv[1]+str(i)+".csv"
	outpath = sys.argv[2]+str(i)+".csv"
	while os.path.exists(inpath):
		
		data = pd.read_csv(inpath, header=0)

		odom_pos_x = data['odom_pos_x']
		odom_pos_x = odom_pos_x + np.random.normal(0, 0.02, odom_pos_x.shape)

		odom_pos_y = data['odom_pos_y']
		odom_pos_y = odom_pos_y + np.random.normal(0, 0.02, odom_pos_x.shape)

		odom_quat_x = data['odom_quat_x']
		odom_quat_y = data['odom_quat_y']
		odom_quat_z = data['odom_quat_z']
		odom_quat_w = data['odom_quat_w']

		odom_vel_x = data['odom_vel_x']
		odom_vel_x = odom_vel_x + np.random.normal(0, 0.02, odom_pos_x.shape)

		odom_vel_y = data['odom_vel_y']
		odom_vel_y = odom_vel_y + np.random.normal(0, 0.02, odom_pos_x.shape)

		odom_ang_z = data['odom_ang_z']
		odom_ang_z = odom_ang_z + np.random.normal(0, 0.02, odom_pos_x.shape)

		imu_quat_x = data['imu_quat_x']
		imu_quat_y = data['imu_quat_y']
		imu_quat_z = data['imu_quat_z']
		imu_quat_w = data['imu_quat_w']

		imu_ang_x = data['imu_ang_x']
		imu_ang_x = imu_ang_x + np.random.normal(0, 0.006, odom_pos_x.shape)

		imu_ang_y = data['imu_ang_y']
		imu_ang_y = imu_ang_y + np.random.normal(0, 0.006, odom_pos_x.shape)

		imu_ang_z = data['imu_ang_z']
		imu_ang_z = imu_ang_z + np.random.normal(0, 0.006, odom_pos_x.shape)

		imu_accel_x = data['imu_accel_x']
		imu_accel_x = imu_accel_x + np.random.normal(0, 0.001, odom_pos_x.shape)
		
		imu_accel_y = data['imu_accel_y']
		imu_accel_y = imu_accel_y + np.random.normal(0, 0.001, odom_pos_x.shape)

		
		new_csv = pd.DataFrame({'odom_pos_x' : odom_pos_x,
					'odom_pos_y' :  odom_pos_y,
					'odom_quat_x' :  odom_quat_x,
					'odom_quat_y' : odom_quat_y,
					'odom_quat_z' :  odom_quat_z,
					'odom_quat_w' : odom_quat_w,
					'odom_vel_x' : odom_vel_x,
					'odom_vel_y' : odom_vel_y,
					'odom_ang_z' : odom_ang_z,
					'imu_quat_x' : imu_quat_x,
					'imu_quat_y' : imu_quat_y,
					'imu_quat_z' : imu_quat_z,
					'imu_quat_w' : imu_quat_w,
					'imu_ang_x' :  imu_ang_x,
					'imu_ang_y' : imu_ang_y,
					'imu_ang_z' : imu_ang_z,
					'imu_accel_x' : imu_accel_x,
					'imu_accel_y' :  imu_accel_y })

		new_csv.to_csv(outpath, columns = myFields,index=False)
		i = i + 1
		inpath = sys.argv[1]+str(i)+".csv"
		outpath = sys.argv[2]+str(i)+".csv"


if __name__ == '__main__':
	add_noise()