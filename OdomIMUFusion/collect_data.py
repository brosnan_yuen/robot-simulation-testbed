import rospy
import csv
import thread
import time
import random
import sys
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
from std_srvs.srv import Empty

new_odom = Odometry()
new_imu = Imu()

start_data = True
start_vel = False

vx = random.uniform(-1.0, 1.0) # X Vel
vy = random.uniform(-1.0, 1.0) # Y Vel
th = random.uniform(-0.2, 0.2) # Theta


def odom_callback(data):
	global new_odom
	new_odom = data

def imu_callback(data):
	global new_imu
	new_imu = data


def stop_sim():
	global start_data
	global start_vel
	while not rospy.is_shutdown():
		time.sleep(40)
		start_data = False


def update_random():
	global vx
	global vy
	global th
	while not rospy.is_shutdown():
		time.sleep(random.randint(1,40))
		if random.randint(0,1) == 0:
			vx = random.uniform(-1.0, 1.0) # X Vel
			vy = random.uniform(-1.0, 1.0) # Y Vel
			th = random.uniform(-1.0, 1.0) # Theta
		else:
			vx = random.uniform(-1.0, 1.0) # X Vel
			vy = random.uniform(-1.0, 1.0) # Y Vel
			th = 0 # Theta
		

def cmd_vel():
	global vx
	global vy
	global th 
	global start_data
	global start_vel
	pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=20)
	rate = rospy.Rate(20) # 1Hz
	while not rospy.is_shutdown():
		time.sleep(1)
		if (start_vel == True):
			time.sleep(3)
			while (not rospy.is_shutdown()) and (start_vel == True):
				pub.publish(Twist(Vector3(vx,vy,0),Vector3(0,0,th)))
				rate.sleep()
		else:
			while (not rospy.is_shutdown()) and (start_vel == False):
				pub.publish(Twist(Vector3(0,0,0),Vector3(0,0,0)))
				time.sleep(0.1)

def get_data():
	global start_data
	global start_vel
	global new_odom
	global new_imu
	myFile = open(sys.argv[1], 'w')  
	rospy.init_node('collect_data', anonymous=True)

	rospy.Subscriber("/odom", Odometry, odom_callback, queue_size=110)

	rospy.Subscriber("/mobile_base/sensors/imu_data", Imu, imu_callback, queue_size=110)

	rate = rospy.Rate(80)
	
	myFields = ['odom_pos_x',
			'odom_pos_y' ,
			'odom_quat_x' ,
			'odom_quat_y',
			'odom_quat_z' ,
			'odom_quat_w' ,
			'odom_vel_x' ,
			'odom_vel_y',
			'odom_ang_z' ,
			'imu_quat_x' ,
			'imu_quat_y',
			'imu_quat_z',
			'imu_quat_w',
			'imu_ang_x' ,
			'imu_ang_y' ,
			'imu_ang_z' ,
			'imu_accel_x' ,
			'imu_accel_y' ]
	writer = csv.DictWriter(myFile,fieldnames=myFields)
	writer.writeheader()
	thread1 = thread.start_new_thread( cmd_vel ,() )
	thread2 = thread.start_new_thread( update_random ,() )
	thread3 = thread.start_new_thread( stop_sim ,() )
	while not rospy.is_shutdown() :
		time.sleep(1)
		if (start_data == True):
			start_vel = True
			while (not rospy.is_shutdown()) and (start_data == True) :
				writer.writerow({'odom_pos_x' : new_odom.pose.pose.position.x, 
						'odom_pos_y' :  new_odom.pose.pose.position.y,
						'odom_quat_x' :  new_odom.pose.pose.orientation.x,
						'odom_quat_y' :  new_odom.pose.pose.orientation.y,
						'odom_quat_z' :  new_odom.pose.pose.orientation.z,
						'odom_quat_w' :  new_odom.pose.pose.orientation.w,
						'odom_vel_x' :  new_odom.twist.twist.linear.x,
						'odom_vel_y' :  new_odom.twist.twist.linear.y,
						'odom_ang_z' :  new_odom.twist.twist.angular.z,
						'imu_quat_x' :  new_imu.orientation.x,
						'imu_quat_y' :  new_imu.orientation.y,
						'imu_quat_z' :  new_imu.orientation.z,
						'imu_quat_w' :  new_imu.orientation.w,
						'imu_ang_x' :  new_imu.angular_velocity.x,
						'imu_ang_y' :  new_imu.angular_velocity.y,
						'imu_ang_z' :  new_imu.angular_velocity.z,
						'imu_accel_x' :  new_imu.linear_acceleration.x,
						'imu_accel_y' :  new_imu.linear_acceleration.y })
				rate.sleep()
		else:
			start_vel == False
			myFile.close()
			thread1.exit()
			thread2.exit()
			thread3.exit()
			sys.exit()
	thread1.exit()
	thread2.exit()
	thread3.exit()
	sys.exit()


if __name__ == '__main__':
	get_data()