import subprocess
import time

def run_sim():
	i = 0
	while (i < 10000):
		print "Start Sim "+str(i)
		subprocess.call("sh ./sim.sh", shell=True)
		time.sleep(12)
		collect = subprocess.Popen("python ./collect_data.py ./raw_data/"+str(i)+".csv", shell=True)
		time.sleep(41)
		collect.kill()
		time.sleep(5)
		subprocess.call("sh ./kill.sh", shell=True)
		time.sleep(5)
		print "Stop Sim"
		i = i + 1


if __name__ == '__main__':
	run_sim()